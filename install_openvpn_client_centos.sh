#!/bin/bash
if [ "$#" -ne 4 ]; then
    echo "Please use like: $BASH_SOURCE 192.168.100.66 client_name root qweqwe"
    echo "where 192.168.100.66 - public ip address of OpenVpn server, root - login to server, qweqwe - password"
    exit 0
fi
echo "=============================="
echo "start install and configure OpenVpn client"
echo "=============================="
CUR_PATH=`pwd`

echo "=============================="
echo "install openvpn"
echo "=============================="
rpm -ivh http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
yum install -y openvpn sshpass

echo "=============================="
echo "generate certificates"
echo "=============================="
sshpass -p "$4" ssh -oStrictHostKeyChecking=no "$3@$1" 'cd /etc/openvpn/easy-rsa && source ./vars && ./pkitool '"$2"

echo "=============================="
echo "copy certificates"
echo "=============================="
sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/ca.crt /etc/openvpn
sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/$2.crt /etc/openvpn
sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/$2.key /etc/openvpn
sshpass -p "$4" scp -r "$3@$1":/etc/openvpn/easy-rsa/keys/ta.key /etc/openvpn

echo "=============================="
echo "configure client"
echo "=============================="
cp /usr/share/doc/openvpn-*/sample/sample-config-files/client.conf /etc/openvpn
sed -i -e 's/remote my-server-1 1194/remote '"$1"' 1194/g' /etc/openvpn/client.conf
sed -i -e 's/cert client.crt/cert '"$2"'.crt/g' /etc/openvpn/client.conf
sed -i -e 's/key client.key/key '"$2"'.key/g' /etc/openvpn/client.conf


echo "=============================="
echo "start OpenVpn"
echo "=============================="
chkconfig openvpn on
service openvpn start

cd $CUR_PATH
