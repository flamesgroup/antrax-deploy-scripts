#!/bin/bash
if [ "$#" -ne 3 ]; then
    echo "Please use like: $BASH_SOURCE eth0 192.168.100.66 10.8.0.0"
    echo "where 192.168.100.66 - ip address, eth0 interface for ip address, 10.8.0.0 - op address for OpenVpn server"
    exit 0
fi
echo "=============================="
echo "start install and configure OpenVpn server for ip: $2, interface: $1, OpenVpn ip: $3"
echo "=============================="
CUR_PATH=`pwd`

echo "=============================="
echo "install openvpn and easy-rsa"
echo "=============================="
rpm -ivh http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
yum install -y openvpn easy-rsa

echo "=============================="
echo "generate certificates"
echo "=============================="
mkdir -p /etc/openvpn/easy-rsa/keys
cp -rf /usr/share/easy-rsa/2.0/* /etc/openvpn/easy-rsa
cp /etc/openvpn/easy-rsa/openssl-1.0.0.cnf /etc/openvpn/easy-rsa/openssl.cnf
cd /etc/openvpn/easy-rsa
source ./vars
./clean-all
echo "=============================="
echo "init ca certificate"
echo "=============================="
./pkitool --initca
echo "=============================="
echo "generate certificate for server"
echo "=============================="
./pkitool --server server
./build-dh
openvpn --genkey --secret keys/ta.key

echo "=============================="
echo "configure OpenVpn server"
echo "=============================="
cp /usr/share/doc/openvpn-*/sample/sample-config-files/server.conf /etc/openvpn
sed -i -e 's/server 10.8.0.0 255.255.255.0/server '"$3"' 255.255.255.0/g' /etc/openvpn/server.conf
sed -i -e 's/;local a.b.c.d/local '"$2"'/g' /etc/openvpn/server.conf
sed -i -e 's/;push "redirect-gateway def1 bypass-dhcp"/push "redirect-gateway def1 bypass-dhcp"/g' /etc/openvpn/server.conf
sed -i -e 's/ca ca.crt/ca \/etc\/openvpn\/easy-rsa\/keys\/ca.crt/g' /etc/openvpn/server.conf
sed -i -e 's/cert server.crt/cert \/etc\/openvpn\/easy-rsa\/keys\/server.crt/g' /etc/openvpn/server.conf
sed -i -e 's/key server.key/key \/etc\/openvpn\/easy-rsa\/keys\/server.key/g' /etc/openvpn/server.conf
sed -i -e 's/dh dh2048.pem/dh \/etc\/openvpn\/easy-rsa\/keys\/dh2048.pem/g' /etc/openvpn/server.conf
sed -i -e 's/tls-auth ta.key 0/tls-auth \/etc\/openvpn\/easy-rsa\/keys\/ta.key 0/g' /etc/openvpn/server.conf
sed -i -e 's/;client-to-client/client-to-client/g' /etc/openvpn/server.conf
sed -i -e 's/;user nobody/user nobody/g' /etc/openvpn/server.conf
sed -i -e 's/;group nobody/group nobody/g' /etc/openvpn/server.conf
sed -i -e 's/;log         openvpn.log/log         \/var\/log\/openvpn.log/g' /etc/openvpn/server.conf

echo "=============================="
echo "configuring network"
IP_TABLES_BAKUP="/etc/sysconfig/iptables.bak.$(date +%Y%m%d%H%M%S)"
echo "create iptabeles backup file: $IP_TABLES_BAKUP"
echo "=============================="
cp -pv /etc/sysconfig/iptables $IP_TABLES_BAKUP
echo "=============================="
echo "restart iptables"
echo "=============================="
service iptables restart
echo "=============================="
echo "configure iptables"
echo "=============================="
iptables -I INPUT 1 -p udp --dport 1194 -j ACCEPT
iptables -I FORWARD 1 -i $1 -o tun0 -j ACCEPT
iptables -I FORWARD 1 -i tun0 -o $1 -j ACCEPT
iptables -t nat -A POSTROUTING -s $3/24 -o $1 -j SNAT --to-source $2
iptables -t nat -A POSTROUTING -s $3/24 -o $1 -j MASQUERADE
service iptables save

sed -i -e 's/net.ipv4.ip_forward = 0/net.ipv4.ip_forward = 1/g' /etc/sysctl.conf
sysctl -p

echo "=============================="
echo "start OpenVpn"
echo "=============================="
chkconfig openvpn on
service openvpn start

cd $CUR_PATH
 
