#!/bin/bash

# ------------------------------------------
# Antrax software server installation script

# Aleksei Shentiakov
# a.shentiakov@flamesgroup.com

# last update 31.08.2017
# ------------------------------------------


distro_version='unknown'

antrax_version_major=1
antrax_version_minor=16
antrax_version_patch=5

DEBUG=0

whattoinstall=1
whattoinstallname=''

# background job progress spinner
_spinner () {
local LAST_BACKGROUND_JOB_PID=$!

i=1
sp="/-\|"

echo -n ' '

while [ -d /proc/$LAST_BACKGROUND_JOB_PID ]
do
	printf "\b${sp:i++%${#sp}:1}"
	sleep 0.1s
done

echo ''
}

_wait_job_end () {
local LAST_BACKGROUND_JOB_PID=$!

while [ -d /proc/$LAST_BACKGROUND_JOB_PID ]
do
	sleep 5s
done
}

_wait () {
if [ "$DEBUG" -eq "1" ]
then
_wait_job_end
else
_spinner
fi
}

# $1 trace-file
_trace_begin () {
if [ "$DEBUG" -eq "1" ]
then
local XTRACE_FILE="$1.xtrace"
exec 100>>"$XTRACE_FILE"
BASH_XTRACEFD=100
set -x
fi
}

_trace_end () {
if [ "$DEBUG" -eq "1" ]
then
set +x
fi
}

comment_char='#'

# _comment_out_line pattern file
_comment_out_line () {
sed -i -e "/$1/ s/^${comment_char}*/${comment_char}/" "$2"
}

# _header configfile
# uses comment_char
_header () {
echo -e "\n" >> $1
echo "$comment_char --------------------------------" >> $1
echo "$comment_char Antrax(" >> $1
echo "$comment_char $ME" >> $1
echo "$comment_char $(date)" >> $1
echo "$comment_char" >> $1
}

# _footer configfile
# uses comment_char
_footer () {
echo "$comment_char" >> $1
echo "$comment_char )Antrax" >> $1
echo "$comment_char --------------------------------" >> $1
}

# _add_to_config new-string config-file
_add_to_config () {
_header $2
echo -e "$1" >> $2
_footer $2
}

# _log message logfile
_log () {
echo -e "\n###### $(date) --- $1" >> $2
}

# _milestone_success milestone
_milestone_success () {
echo 'success' > "$1.milestone"
}

# _milestone_fail milestone
_milestone_fail () {
echo 'failed' > "$1.milestone"
}

# _is_milestone_success milestone
_is_milestone_success () {
if [ -f "$1.milestone" ]
then
	milestone_result=$(cat "$1.milestone")

	if [ "$milestone_result" == "success" ]
	then
		#echo "eq succ"
		return 0
	else
		#echo "not eq succ"
		return 1
	fi
else
	return 1
fi
}

# ---------------------

get_distro_version () {
local version=$(cat /proc/version)

if [[ "$version" =~ 'Red Hat' ]]
then
	distro_version="redhat"
elif [[ "$version" =~ 'debian' ]]
then
	distro_version="debian"
elif [[ "$version" =~ 'Ubuntu/Linaro' ]]
then
	distro_version="debian"		# box
else
	distro_version="unknown"
fi
}

# ----------------------
# RED HAT
# ----------------------

# $1 - milestone
rh_disable_selinux () {
echo "disabling selinux..."

local LOG_FILE="$1.log"
local CONFIG_FILE="/etc/selinux/config"

_trace_begin $1

comment_char='#'
_comment_out_line "SELINUX=" $CONFIG_FILE
_add_to_config "SELINUX=disabled" $CONFIG_FILE

_trace_end

# check result
if grep -q "SELINUX=disabled" $CONFIG_FILE
then
# success
_milestone_success $1
echo -e "selinux disabled.\n"

local logcontent="
$CONFIG_FILE
SELINUX=disabled
"
_log "$logcontent" $LOG_FILE
else
# fail
_milestone_fail $1
echo -e "selinux not disabled.\n"
fi
}

# ----------------------

# $1 - milestone
rh_disable_iptables () {
echo "disabling iptables..."

local LOG_FILE="$1.log"

_trace_begin $1

# disable iptables
/etc/init.d/iptables stop &>> $LOG_FILE
chkconfig iptables off &>> $LOG_FILE

# disable ip6tables
/etc/init.d/ip6tables stop &>> $LOG_FILE
chkconfig ip6tables off &>> $LOG_FILE

_trace_end

# check result
ip_tables_status=$(service iptables status)

if echo "$ip_tables_status" | grep -q "not running"
then
	_milestone_success $1
	echo -e "iptables disabled.\n"
else
	_milestone_fail $1
	echo -e "iptables not disabled.\n"
fi
}

# ----------------------

# $1 - milestone
rh_install_packages () {
echo "installing packages..."

local LOG_FILE="$1.log"
echo "" > $LOG_FILE

_trace_begin $1

yum -y install \
sudo \
vim mc nc \
wget \
ntpdate \
unzip \
zlib-devel \
setuptool \
system-config-securitylevel-tui \
authconfig \
system-config-network-tui \
ntsysv \
autoconf gcc make \
&>> $LOG_FILE \
&

_wait

_trace_end

# check result
yum list installed ntpdate &> /dev/null

if [ "$?" -eq "0" ]
then
# found
_milestone_success $1
echo -e "packages installed.\n"
else
# not found
_milestone_fail $1
echo -e "packages not installed.\n"
fi
}

# ----------------------

# $1 - milestone
rh_sync_date_time () {
echo "synchronizing date and time..."

local LOG_FILE="$1.log"
local NTP_SERVER="ntp.time.in.ua"

_trace_begin $1

ntpdate "$NTP_SERVER" &>> $LOG_FILE
RESULT=$?

_trace_end

# check result
if [ "$RESULT" -eq "0" ]
then
# success
_milestone_success $1
echo -e "date and time synchronized.\n"
else
# fail
_milestone_fail $1
echo -e "date and time not synchronized.\n"
fi
}

# ----------------------

# $1 - milestone
rh_set_hostname () {
echo "setting hostname..."

local new_hostname=''
read -p "new hostname: " new_hostname

local LOG_FILE="$1.log"
local CONFIG_FILE_HOSTS="/etc/hosts"
local CONFIG_FILE_NETWORK="/etc/sysconfig/network"

_trace_begin $1

# hostname new_hostname
hostname "$new_hostname"
_log "hostname $new_hostname" $LOG_FILE

# /etc/hosts
_add_to_config "127.0.0.1 $new_hostname" $CONFIG_FILE_HOSTS
_log "127.0.0.1 $new_hostname -> $CONFIG_FILE_HOSTS" $LOG_FILE

# /etc/sysconfig/network
comment_char='#'
_comment_out_line "HOSTNAME=" $CONFIG_FILE_NETWORK
_add_to_config "HOSTNAME=$new_hostname" $CONFIG_FILE_NETWORK
_log "HOSTNAME=$new_hostname -> $CONFIG_FILE_NETWORK" $LOG_FILE

_trace_end

# check result
if grep -q "127.0.0.1 $new_hostname" $CONFIG_FILE_HOSTS && \
grep -q "HOSTNAME=$new_hostname" $CONFIG_FILE_NETWORK
then
# success
_milestone_success $1
echo -e "hostname set.\n"
else
# fail
_milestone_fail $1
echo -e "hostname not set.\n"
fi
}

# ----------------------

# $1 - milestone
rh_update_sysctl () {
echo "updating sysctl..."

local LOG_FILE="$1.log"
local CONFIG_FILE="/etc/sysctl.conf"

_trace_begin $1

comment_char='#'
_comment_out_line 'net\.ipv4\.ip_forward' $CONFIG_FILE
_comment_out_line 'net\.ipv6\.conf\.all\.disable_ipv6' $CONFIG_FILE
_comment_out_line 'net\.ipv6\.conf\.default\.disable_ipv6' $CONFIG_FILE

local UPDATE="
net.ipv4.ip_forward = 1
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
"
_add_to_config "$UPDATE" $CONFIG_FILE

_log "$UPDATE" $LOG_FILE

# apply settings
sysctl -p &>> $LOG_FILE

_trace_end

# check result
if grep -q "net.ipv6.conf.all.disable_ipv6 = 1" $CONFIG_FILE
then
# success
_milestone_success $1
echo -e "sysctl updated.\n"
else
# fail
_milestone_fail $1
echo -e "sysctl not updated.\n"
fi
}

# ----------------------

# $1 - milestone
rh_update_antrax_user_limits () {
echo "updating antrax user limits..."

local LOG_FILE="$1.log"

_trace_begin $1

local CONFIG_FILE_LIMITS="/etc/security/limits.conf"
local CONFIG_FILE_PROFILE="/etc/profile"
local CONFIG_FILE_LOGIN="/etc/pam.d/login"

comment_char="#"

local UPDATE1='
antrax soft nofile 4096
antrax hard nofile 16384
'
_add_to_config "$UPDATE1" $CONFIG_FILE_LIMITS
_log "$UPDATE1 -> $CONFIG_FILE_LIMITS" $LOG_FILE

local UPDATE2='
if [ $USER = "antrax" ]
then
ulimit -n 16384
ulimit -c unlimited
fi
'
_add_to_config "$UPDATE2" $CONFIG_FILE_PROFILE
_log "$UPDATE2 -> $CONFIG_FILE_PROFILE" $LOG_FILE

local UPDATE3='
session required pam_limits.so
'
_add_to_config "$UPDATE3" $CONFIG_FILE_LOGIN
_log "$UPDATE3 -> $CONFIG_FILE_LOGIN" $LOG_FILE

_trace_end

# check result
if grep -q 'antrax soft nofile 4096' $CONFIG_FILE_LIMITS && \
grep -q 'if \[ \$USER = "antrax" \]' $CONFIG_FILE_PROFILE && \
grep -q 'session required pam_limits.so' $CONFIG_FILE_LOGIN
then
# success
_milestone_success $1
echo -e "antrax user limits updated.\n"
else
# fail
_milestone_fail $1
echo -e "antrax user limits not updated.\n"
fi
}

# ----------------------

# $1 - milestone
rh_install_java () {
echo "installing java..."

local LOG_FILE="$1.log"

java -version &> /dev/null

if [ $? -eq 127 ]
then
	# download Oracle java
	echo "downloading jdk rpm..."
	
	# get latest x64 rpm link
	local latest_x64_rpm=$(wget -q -O - http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html | sed -n 's/^.*\(http:.*x64\.rpm\).*$/\1/p')
	_log "latest_x64_rpm = $latest_x64_rpm" $LOG_FILE
	
	# download latest x64 rpm
	wget --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" \
	$latest_x64_rpm \
	&>> $LOG_FILE &
	
	_wait
	
	# install Oracle java
	echo "installing jdk rpm..."
	
	rpm -ihv ./jdk-8u144-linux-x64.rpm &>> $LOG_FILE &
	
	_wait
	
	# check result
	java -version &> /dev/null
	
	if [ $? -ne 127 ]
	then
		_milestone_success $1
		echo -e "java installed.\n"
		
		JAVA_HOME="/usr/java/latest"
		export JAVA_HOME
		echo 'export JAVA_HOME="/usr/java/latest"' >> /etc/profile
	else
		_milestone_fail $1
		echo -e "java not installed.\n"
	fi
else
	echo -e "java is already installed.\n"
	_log "java is already installed" $LOG_FILE
	_milestone_success $1
fi
}

# ----------------------

# $1 - milestone
rh_install_jsvc () {
echo "installing jsvc..."

local LOG_FILE="$1.log"

# check if jsvc installed
jsvc --help  &> /dev/null

if [ $? -eq 127 ]
then
	# jsvc is not installed here, install it now

	# download commons daemon src
	wget https://www.apache.org/dist/commons/daemon/source/commons-daemon-1.0.15-src.tar.gz &>> $LOG_FILE
	
	# uncompress sources
	tar -zxf commons-daemon-1.0.15-src.tar.gz &>> $LOG_FILE
	
	# save current directory
	local CURRENT_DIR="$PWD"
	
	# change directory
	cd ./commons-daemon-1.0.15-src/src/native/unix
	
	echo "configuring..."
	./configure &>> $LOG_FILE &
	
	_wait
	
	echo "building..."
	make &>> $LOG_FILE &
	
	_wait
	
	# copy compiled binaries
	cp ./jsvc /usr/bin/
	
	# restore current directory
	cd "$CURRENT_DIR"
	
	# check result
	jsvc --help  &> /dev/null
	
	if [ $? -ne 127 ]
	then
	# success
	_milestone_success $1
	echo -e "jsvc installed.\n"
	else
	# fail
	_milestone_fail $1
	echo -e "jsvc not installed.\n"
	fi
else
	echo "jsvc is already installed."
	_milestone_success $1
fi
}

# ----------------------

# $1 - milestone
rh_install_postgres () {
echo "installing postgres..."

local LOG_FILE="$1.log"
local CONFIG_FILE_HBA="/opt/postgresql/data/pg_hba.conf"
local CONFIG_FILE_POSTGRES="/opt/postgresql/data/postgresql.conf"

# add postgres repo
rpm -Uvh http://yum.postgresql.org/9.5/redhat/rhel-6-x86_64/pgdg-redhat95-9.5-2.noarch.rpm &>> $LOG_FILE

# install postgres server
echo "installing postgresql95 server..."
yum -y install rpm-devel postgresql95-server postgresql95 &>> $LOG_FILE &
_wait

# /opt
mkdir -p /opt/postgresql/data
chown -R postgres:postgres /opt/postgresql

echo "PGDATA=/opt/postgresql/data
PGLOG=/opt/postgresql/pgstartup.log" > /etc/sysconfig/pgsql/postgresql-9.5

# initdb
echo "init db..."
/etc/init.d/postgresql-9.5 initdb &>> $LOG_FILE &
_wait

# update config
comment_char="#"
_add_to_config "host all all  0.0.0.0/0  md5" $CONFIG_FILE_HBA

_comment_out_line 'bytea_output' $CONFIG_FILE_POSTGRES
local update="
listen_addresses = '*'
bytea_output = 'escape'
"
_add_to_config "$update" $CONFIG_FILE_POSTGRES

#
sed -i -E 's/host(.+)127.0.0.1\/32(.+)ident/host all all 127.0.0.1\/32 md5/g' /opt/postgresql/data/pg_hba.conf
sed -i -e "s/^#wal_level =.*$/wal_level = logical/" /opt/postgresql/data/postgresql.conf

# autorun
chkconfig postgresql-9.5 on

# start postgres server
echo "starting postgres server..."
/etc/init.d/postgresql-9.5 start

# check result
local pgstatus=$(/etc/init.d/postgresql-9.5 status)

if echo "$pgstatus" | grep -Fq "is running"
then
# postgres service ok

# create antrax user
sudo -u postgres createuser -d -R -s -P antrax

# create antrax database
sudo -u postgres createdb -T template1 -E UTF8 -O antrax antrax
	
_milestone_success $1
echo -e "postgres installed.\n"
else
# postgress is not running
_milestone_fail $1
echo -e "postgres not installed.\n"
fi
}

# ----------------------

# $1 - milestone
rh_install_yate () {
echo "install yate:"

local LOG_FILE="$1.log"
local CONFIG_FILE_INIT="/etc/init.d/yate"

# add yum repo
local repo="
[yate]
name=Yate Yum Repo
baseurl=https://antrax-repo:antraxSupport@rpm.antrax.mobi/6/yate/
enabled=1
# not secure
gpgcheck=0
sslverify=1
"

echo -e "$repo" >> "/etc/yum.repos.d/yate-antrax.repo"

# install yate from repo
echo "installing yate..."
yum -y install yate\* &>> $LOG_FILE &
_wait

# check result
if [ -f "$CONFIG_FILE_INIT" ]
then
# yate config file found

# add to autorun
chkconfig --add yate
	
comment_char='#'	
_comment_out_line "OPTS=" $CONFIG_FILE_INIT
_add_to_config 'OPTS="-F -s -r -l /opt/yate/var/log/yate -vvvvvv -Df"' $CONFIG_FILE_INIT

# save milestone
_milestone_success $1
echo -e "yate installed.\n"

# run yate
/etc/init.d/yate start

else
# yate config file not found
_milestone_fail $1
echo -e "yate not installed.\n"
fi
}

# ----------------------

# $1 - "cs" or "vs" or "ss"
# $2 - "redhat" or "debian"
config_antrax () {
echo "configuring antrax..."

# commons
local CONFIG_FILE_COMMONS_SN="/opt/antrax/commons/etc/server.name"
local CONFIG_FILE_COMMONS_CSU="/opt/antrax/commons/etc/control.server.url"

# control server
local CONFIG_FILE_CS_CSP="/opt/antrax/control-server/etc/control-server.properties"

# --- commons ---

# set server name
local server_name
read -p "commons -> server.name = " server_name
echo "$server_name" > $CONFIG_FILE_COMMONS_SN

# set control server url
local control_server_url
read -p "commons -> control.server.url = " control_server_url
echo "rmi://${control_server_url}:1222" > $CONFIG_FILE_COMMONS_CSU

case $1 in

# ----- control server ------
cs)

# set rmi server hostname
local rmi_server_hostname
read -p "control-server -> control-server.properties -> rmi.server.hostname = " rmi_server_hostname

sed -i "s/rmi\.server\.hostname.*/rmi\.server\.hostname=${rmi_server_hostname}/" $CONFIG_FILE_CS_CSP

;;

# ----- voice server ------
vs)
:
;;

# ----- sim server ------
ss)
:
;;

esac

# add antrax daemon to autorun

case $2 in

redhat)
chkconfig --add antrax.all
;;

debian)
update-rc.d antrax.all defaults
;;

esac

}

# ----------------------

# $1 - milestone
# $2 - "cs" or "vs" or "ss"
rh_install_antrax () {

# check if JAVA_HOME is defined
if [ -z $JAVA_HOME ]
then
echo -e "JAVA_HOME is empty!"
exit 1
fi

echo "installing antrax $2 ..."

local LOG_FILE="$1.log"

# add antrax repo
local repo="
[antrax]
name=Antrax Yum Repo
baseurl=https://antrax-repo:antraxSupport@rpm.antrax.mobi/6/antrax/release/${antrax_version_major}.${antrax_version_minor}.${antrax_version_patch}
enabled=1
# not secure
gpgcheck=0
sslverify=1
"

echo -e "$repo" >> "/etc/yum.repos.d/antrax.repo"

case $2 in

cs)

# ------------ install control server --------------

yum -y install \
antrax.commons.package \
antrax.control-server.package \
antrax.gui-client-webstart.package \
antrax.tools.package &>> $LOG_FILE &

_wait

# check result

yum list installed antrax.control-server.package &> /dev/null

if [ $? -eq 0 ]
then
config_antrax "cs" $distro_version
_milestone_success $1
echo -e "antrax cs installed."
else
_milestone_fail $1
echo -e "antrax cs not installed."
fi

;;

vs)

# ------------ install voice server --------------

yum -y install \
antrax.commons.package \
antrax.voice-server.package \
antrax.tools.package &>> $LOG_FILE &

_wait

# check result

yum list installed antrax.voice-server.package &> /dev/null

if [ $? -eq 0 ]
then
config_antrax "vs" $distro_version
_milestone_success $1
echo -e "antrax voice server installed."
else
_milestone_fail $1
echo -e "antrax voice server not installed."
fi

;;

ss)

# ---------------- install sim server -----------------

yum -y install \
antrax.commons.package \
antrax.sim-server.package \
antrax.tools.package &>> $LOG_FILE &

_wait

# check result

yum list installed antrax.sim-server.package &> /dev/null

if [ $? -eq 0 ]
then
config_antrax "ss" $distro_version
_milestone_success $1
echo -e "antrax ss installed."
else
_milestone_fail $1
echo -e "antrax ss not installed."
fi

;;

esac

}

# ----------------------

rh_install_zabbix () {
rpm -ivh http://repo.zabbix.com/zabbix/2.4/rhel/6/x86_64/zabbix-release-2.4-1.el6.noarch.rpm

yum -y install mysql-server zabbix-server-mysql zabbix-web-mysql zabbix-agent

/sbin/service mysqld start

mysql -e 'create database zabbix character set utf8 collate utf8_bin;grant all privileges on zabbix.* to zabbix@localhost identified by 'zabbix';'

mysql -uroot zabbix < /usr/share/doc/zabbix-server-mysql-2.4.8/create/schema.sql
mysql -uroot zabbix < /usr/share/doc/zabbix-server-mysql-2.4.8/create/images.sql
mysql -uroot zabbix < /usr/share/doc/zabbix-server-mysql-2.4.8/create/data.sql

echo '
DBHost=localhost
DBPassword=zabbix
' >> /etc/zabbix/zabbix_server.conf

/sbin/iptables -A INPUT 1 -p tcp --dport 10050 -j ACCEPT 
/sbin/iptables -A INPUT 1 -p udp --dport 10050 -j ACCEPT 
/sbin/iptables -A INPUT 1 -p tcp --dport 10051 -j ACCEPT 
/sbin/iptables -A INPUT 1 -p udp --dport 10051 -j ACCEPT

echo 'php_value date.timezone Europe/Riga' >> /etc/httpd/conf.d/zabbix.conf

/sbin/iptables -A INPUT -p tcp --dport 80 -j ACCEPT

service mysqld restart
service httpd restart
service zabbix-server restart
}

# ----------------------

install_for_redhat () {
local centos_version_major
local centos_version_minor

echo -e "red hat antrax installation...\n"

# check centos version

centos_version_major=$(sed 's/^.*\(.\)\..*$/\1/' /etc/redhat-release)
centos_version_minor=$(sed 's/^.*\.\(.\).*$/\1/' /etc/redhat-release)

if [ "$centos_version_major" -ne "6" ]
then
echo "major centos version not equal 6!"
exit 1
fi

if [ "$centos_version_minor" -gt "8" ]
then
echo "centos ${centos_version_major}.${centos_version_minor} downgrade is required!"
exit 1
fi

#: <<'RH_DISABLE_IPTABLES'
# disable iptables
milestone='disable-iptables'

if ! _is_milestone_success "$milestone"
then
	rh_disable_iptables "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_DISABLE_IPTABLES


#: <<'RH_DISABLE_SELINUX'
# disable selinux
milestone='disable-selinux'

if ! _is_milestone_success "$milestone"
then
	rh_disable_selinux "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_DISABLE_SELINUX


#: <<'RH_INSTALL_PACKAGES'
# install required packages
milestone='install-packages'
	
if ! _is_milestone_success "$milestone"
then
	rh_install_packages "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_INSTALL_PACKAGES


#: <<'RH_SYNC_DATETIME'
# sync date time
milestone='sync-date-time'

if ! _is_milestone_success "$milestone"
then
	rh_sync_date_time "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_SYNC_DATETIME


#: <<'RH_SET_HOSTNAME'
# set hostname
milestone='set-hostname'

if ! _is_milestone_success "$milestone"
then
	rh_set_hostname "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_SET_HOSTNAME


#: <<'RH_UPDATE_SYSCTL'
# update sysctl
milestone='update-sysctl'

if ! _is_milestone_success "$milestone"
then
	rh_update_sysctl "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_UPDATE_SYSCTL


#: <<'RH_UPDATE_USER_LIMITS'
# update antrax user limits
milestone='update-antrax-user-limits'

if ! _is_milestone_success "$milestone"
then
	rh_update_antrax_user_limits "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_UPDATE_USER_LIMITS


#: <<'RH_INSTALL_JAVA_JSVC'
# install java
milestone='install-java'

if ! _is_milestone_success "$milestone"
then
	rh_install_java "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi

# install jsvc
milestone='install-jsvc'

if ! _is_milestone_success "$milestone"
then
	rh_install_jsvc "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_INSTALL_JAVA_JSVC



case $whattoinstall in
1)

# ----------- install control server ------------


#: <<'RH_INSTALL_POSTGRES'
# install postgres
milestone='install-postgres'

if ! _is_milestone_success "$milestone"
then
	rh_install_postgres "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_INSTALL_POSTGRES


#: <<'RH_INSTALL_YATE'
# install yate
milestone='install-yate'

if ! _is_milestone_success "$milestone"
then
	rh_install_yate "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_INSTALL_YATE


#: <<'RH_INSTALL_ANTRAX'
# install antrax
milestone='install-antrax'

if ! _is_milestone_success "$milestone"
then
	rh_install_antrax "$milestone" "cs"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_INSTALL_ANTRAX

;;


2)

# ----------- install voice server -----------

#: <<'RH_INSTALL_ANTRAX_VS'
# install antrax
milestone='install-antrax'

if ! _is_milestone_success "$milestone"
then
	rh_install_antrax "$milestone" "vs"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_INSTALL_ANTRAX_VS

;;

3)

# ------------- install sim server ------------

#: <<'RH_INSTALL_ANTRAX_SS'
# install antrax
milestone='install-antrax'

if ! _is_milestone_success "$milestone"
then
	rh_install_antrax "$milestone" "ss"
else
	echo -e "[$milestone] skipped\n"
fi
#RH_INSTALL_ANTRAX_SS

;;

esac

echo -e "\nredhat antrax installation finished.\n"
}

# ----------------------



# ----------------------
# DEBIAN
# ----------------------

# $1 - milestone
deb_install_packages () {

echo "installing packages..."

local LOG_FILE="$1.log"
echo "" > $LOG_FILE

_trace_begin $1

apt-get update &> /dev/null

apt-get -y install \
mc \
apt-transport-https \
ntpdate \
netselect-apt \
autoconf \
gcc \
make \
sudo \
sysv-rc-conf \
nano \
wget \
openssh-server openssh-client \
zip zlibc zlib1g zlib1g-dev \
ca-certificates \
&>> $LOG_FILE \
&

_wait

_trace_end

# check result
dpkg -s ntpdate apt-transport-https &> /dev/null

if [ "$?" -eq "0" ]
then
# found
_milestone_success $1
echo -e "packages installed.\n"
else
# not found
_milestone_fail $1
echo -e "packages not installed.\n"
fi
}

# ----------------------

# $1 - milestone
deb_sync_date_time () {
echo "synchronizing date and time..."

local LOG_FILE="$1.log"
local NTP_SERVER="ntp.time.in.ua"

_trace_begin $1

ntpdate "$NTP_SERVER" &>> $LOG_FILE
RESULT=$?

_trace_end

# check result
if [ "$RESULT" -eq "0" ]
then
# success
_milestone_success $1
echo -e "date and time synchronized.\n"
else
# fail
_milestone_fail $1
echo -e "date and time not synchronized.\n"
fi
}

# ----------------------

# $1 - milestone
deb_set_hostname () {
echo "setting hostname..."

local new_hostname=''
read -p "new hostname: " new_hostname

local LOG_FILE="$1.log"
local CONFIG_FILE_HOSTS="/etc/hosts"
local CONFIG_FILE_HOSTNAME="/etc/hostname"

_trace_begin $1

# hostname new_hostname
hostname "$new_hostname"
_log "hostname $new_hostname" $LOG_FILE

# /etc/hosts
_add_to_config "127.0.0.1 $new_hostname" $CONFIG_FILE_HOSTS
_log "127.0.0.1 $new_hostname -> $CONFIG_FILE_HOSTS" $LOG_FILE

# /etc/hostname
echo "$new_hostname" > $CONFIG_FILE_HOSTNAME

_trace_end

# check result
if grep -q "127.0.0.1 $new_hostname" $CONFIG_FILE_HOSTS && \
grep -q "$new_hostname" $CONFIG_FILE_HOSTNAME
then
# success
_milestone_success $1
echo -e "hostname set.\n"
else
# fail
_milestone_fail $1
echo -e "hostname not set.\n"
fi
}

# ----------------------

# $1 - milestone
deb_update_antrax_user_limits () {
echo "updating antrax user limits..."

local LOG_FILE="$1.log"

_trace_begin $1

local CONFIG_FILE_LIMITS="/etc/security/limits.conf"
local CONFIG_FILE_PROFILE="/etc/profile"
local CONFIG_FILE_LOGIN="/etc/pam.d/login"

comment_char="#"

local UPDATE1='
antrax soft nofile 4096
antrax hard nofile 16384
'
_add_to_config "$UPDATE1" $CONFIG_FILE_LIMITS
_log "$UPDATE1 -> $CONFIG_FILE_LIMITS" $LOG_FILE

local UPDATE2='
if [ $USER = "antrax" ]
then
ulimit -n 16384
ulimit -c unlimited
fi
'
_add_to_config "$UPDATE2" $CONFIG_FILE_PROFILE

local UPDATE3='
session required pam_limits.so
'
_add_to_config "$UPDATE3" $CONFIG_FILE_LOGIN

_trace_end

# check result
if grep -q 'antrax soft nofile 4096' $CONFIG_FILE_LIMITS && \
grep -q 'if [ $USER = "antrax" ]' $CONFIG_FILE_PROFILE && \
grep -q 'session required pam_limits.so' $CONFIG_FILE_LOGIN
then
# success
_milestone_success $1
echo -e "antrax user limits updated.\n"
else
# fail
_milestone_fail $1
echo -e "antrax user limits not updated.\n"
fi
}

# ----------------------

# $1 - milestone
deb_update_sysctl () {
echo "updating sysctl..."

local LOG_FILE="$1.log"
local CONFIG_FILE="/etc/sysctl.conf"

_trace_begin $1

comment_char='#'
_comment_out_line 'net\.ipv4\.ip_forward' $CONFIG_FILE

local UPDATE="
net.ipv4.ip_forward = 1
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
"
_add_to_config "$UPDATE" $CONFIG_FILE

_log "$UPDATE" $LOG_FILE

#
sysctl -p &>> $LOG_FILE

_trace_end

# check result
if grep -q "net.ipv4.ip_forward = 1" $CONFIG_FILE
then
# success
_milestone_success $1
echo -e "sysctl updated.\n"
else
# fail
_milestone_fail $1
echo -e "sysctl not updated.\n"
fi
}

# ----------------------

# $1 - milestone
deb_install_java () {

local LOG_FILE="$1.log"

java -version &> /dev/null

if [ $? -eq 127 ]
then
	echo "java not found, installing..."

	# add java repo
	echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" >> /etc/apt/sources.list.d/webupd8team-java.list
	
	# install java
	apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886 &>> $LOG_FILE
	apt-get update &> /dev/null
	apt-get -y install oracle-java8-installer
	
	# check result
	java -version &> /dev/null
	
	if [ $? -ne 127 ]
	then
		# success
		
		_milestone_success $1
		
		echo -e "java installed.\n"
		
		# export JAVA_HOME
		JAVA_HOME="/usr/lib/jvm/java-8-oracle"
		export JAVA_HOME
		echo "export JAVA_HOME=$JAVA_HOME" >> /etc/profile
	else
		# fail
		
		_milestone_fail $1
		
		echo -e "java not installed.\n"
	fi
else
	# java is available, no need to install
	echo -e "java is already installed.\n"
	_log "java is already installed" $LOG_FILE
	_milestone_success $1
fi
}

# ----------------------

# $1 - milestone
deb_install_jsvc () {

local LOG_FILE="$1.log"

jsvc --help  &> /dev/null

if [ $? -eq 127 ]
then
	# jsvc is not available, install it
	echo "installing jsvc..."
	
	apt-get -y install jsvc >> $LOG_FILE
	
	# check result
	jsvc --help  &> /dev/null
	
	if [ $? -ne 127 ]
	then
	# success
	_milestone_success $1
	echo -e "jsvc installed.\n"
	else
	# fail
	_milestone_fail $1
	echo -e "jsvc not installed.\n"
	fi
else
	# jsvc is available, no need to install
	_milestone_success $1
	echo -e "jsvc is already installed.\n"
fi
}

# ----------------------

# $1 - milestone
# $2 - "cs" or "vs" or "ss"
deb_install_antrax () {
local LOG_FILE="$1.log"

echo "installing antrax $whattoinstallname..."

# add antrax repo
echo "deb https://antrax-repo:antraxSupport@deb.antrax.mobi/antrax/release/${antrax_version_major}.${antrax_version_minor}.${antrax_version_patch} jessie main" >> /etc/apt/sources.list.d/antrax.list

apt-get update &> /dev/null

case $2 in

# ---------- install control server ----------
cs)
apt-get -y --allow-unauthenticated install \
antrax.commons.package \
antrax.control-server.package \
antrax.gui-client-webstart.package \
antrax.tools.package \
&>> $LOG_FILE &

_wait

# check result
dpkg -s antrax.commons.package > /dev/null

if [ $? -eq 0 ]
then
# success
config_antrax "cs" $distro_version
_milestone_success $1
echo -e "antrax $whattoinstallname installed."
else
# fail
_milestone_fail $1
echo -e "antrax $whattoinstallname not installed."
fi
;;

# --------- install voice server ----------
vs)
apt-get -y --allow-unauthenticated install \
antrax.commons.package \
antrax.voice-server.* \
antrax.tools.package \
&>> $LOG_FILE &

_wait

# check result
dpkg -s antrax.commons.package > /dev/null

if [ $? -eq 0 ]
then
# success
config_antrax "vs" $distro_version
_milestone_success $1
echo -e "antrax $whattoinstallname installed."
else
# fail
_milestone_fail $1
echo -e "antrax $whattoinstallname not installed."
fi
;;

# ----------- install sim server -----------
ss)
apt-get -y --allow-unauthenticated install \
antrax.commons.package \
antrax.sim-server.* \
antrax.tools.package \
&>> $LOG_FILE &

_wait

# check result
dpkg -s antrax.commons.package > /dev/null

if [ $? -eq 0 ]
then
# success
config_antrax "ss" $distro_version
_milestone_success $1
echo -e "antrax $whattoinstallname installed."
else
# fail
_milestone_fail $1
echo -e "antrax $whattoinstallname not installed."
fi
;;

esac
}

# ----------------------

# $1 - milestone
deb_config_locales () {
echo "configuring locales..."

dpkg-reconfigure locales

_milestone_success $1
echo -e "locales configured."
}

# ----------------------

# $1 - milestone
deb_install_yate () {
local LOG_FILE="$1.log"

local not_installed_msg="yate was not installed because of debian distro."
echo -e "${not_installed_msg}\n"
_log "$not_installed_msg" $LOG_FILE
_milestone_success $1
}

# ----------------------

# $1 - milestone
deb_install_postgres () {
local LOG_FILE="$1.log"

local not_installed_msg="postgres was not installed because of debian distro."
echo -e "${not_installed_msg}\n"
_log "$not_installed_msg" $LOG_FILE
_milestone_success $1
}

# ----------------------

install_for_debian () {
echo -e "installing for debian...\n"

# check debian version
local debian_version_major=$(sed 's/^\(.\).*$/\1/' /etc/debian_version)

echo -n "debian version = "
cat /etc/debian_version

if [ $debian_version_major -ne 8 ]
then
exit 1
fi


#: <<'DEB_INSTALL_PACKAGES'
# install required packages
milestone='install-packages'
	
if ! _is_milestone_success "$milestone"
then
	deb_install_packages "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#DEB_INSTALL_PACKAGES


#: <<'DEB_SYNC_DATETIME'
# sync date time
milestone='sync-date-time'

if ! _is_milestone_success "$milestone"
then
	deb_sync_date_time "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#DEB_SYNC_DATETIME


#: <<'DEB_SET_HOSTNAME'
# set hostname
milestone='set-hostname'

if ! _is_milestone_success "$milestone"
then
	deb_set_hostname "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#DEB_SET_HOSTNAME


#: <<'DEB_UPDATE_SYSCTL'
# update sysctl
milestone='update-sysctl'

if ! _is_milestone_success "$milestone"
then
	deb_update_sysctl "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#DEB_UPDATE_SYSCTL


#: <<'DEB_INSTALL_JAVA_JSVC'
# install java
milestone='install-java'

if ! _is_milestone_success "$milestone"
then
	deb_install_java "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi

# install jsvc
milestone='install-jsvc'

if ! _is_milestone_success "$milestone"
then
	deb_install_jsvc "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#DEB_INSTALL_JAVA_JSVC


#: <<'DEB_CONFIG_LOCALES'
# configure locales
milestone='config-locales'

if ! _is_milestone_success "$milestone"
then
	deb_config_locales "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#DEB_CONFIG_LOCALES


case $whattoinstall in

# ----------- install control server -----------
1)

#: <<'DEB_INSTALL_YATE'
# install yate
milestone='install-yate'
	
if ! _is_milestone_success "$milestone"
then
	deb_install_yate "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#DEB_INSTALL_YATE


#: <<'DEB_INSTALL_POSTGRES'
# install postgresql
milestone='install-postgres'
	
if ! _is_milestone_success "$milestone"
then
	deb_install_postgres "$milestone"
else
	echo -e "[$milestone] skipped\n"
fi
#DEB_INSTALL_POSTGRES


#: <<'DEB_INSTALL_ANTRAX_CS'
# install antrax
milestone='install-antrax'

if ! _is_milestone_success "$milestone"
then
	deb_install_antrax "$milestone" "cs"
else
	echo -e "[$milestone] skipped\n"
fi
#DEB_INSTALL_ANTRAX_CS

;;

# ------------ install voice server ------------
2)

#: <<'DEB_INSTALL_ANTRAX_VS'
# install antrax
milestone='install-antrax'

if ! _is_milestone_success "$milestone"
then
	deb_install_antrax "$milestone" "vs"
else
	echo -e "[$milestone] skipped\n"
fi
#DEB_INSTALL_ANTRAX_VS

;;

# ------------ install sim server -------------
3)

#: <<'DEB_INSTALL_ANTRAX_SS'
# install antrax
milestone='install-antrax'

if ! _is_milestone_success "$milestone"
then
	deb_install_antrax "$milestone" "ss"
else
	echo -e "[$milestone] skipped\n"
fi
#DEB_INSTALL_ANTRAX_SS

;;

esac



}

# ==================

main_func () {

# check architecture
local arch=$(arch)

# 'armv7l' is for ARM CPU architecture in GSM/SIM boxes
if [ "$arch" != 'x86_64' ] && [ "$arch" != 'armv7l' ]
then
echo "x86_64 required!"
exit 1
fi

echo "
-------------------
Antrax installation
-------------------
"

# what kind of antrax server to install
echo "please select antrax server type by pressing the specific digit key"
read -n 1 -p "(1) - control server, (2) - voice server, (3) - sim server, (0) - cancel : " whattoinstall
echo -e "\n"

case $whattoinstall in

1)
whattoinstallname='control server'
;;

2)
whattoinstallname='voice server'
;;

3)
whattoinstallname='sim server'
;;

0)
exit 0
;;

esac

# antrax server version
echo "antrax version selection..."
echo "for example antrax version is 1.16.5 : 1-major, 16-minor, 5-patch"
read -p "please enter antrax major version X.x.x: " antrax_version_major
read -p "please enter antrax minor version x.X.x: " antrax_version_minor
read -p "please enter antrax patch version x.x.X: " antrax_version_patch
echo ""

# check distro version
get_distro_version

case $distro_version in

# this is red hat
"redhat")
echo -e "red hat distro detected\n"
echo -e "please confirm that you want to install"
read -n 1 -p "antrax $whattoinstallname ${antrax_version_major}.${antrax_version_minor}.${antrax_version_patch} for red hat? (y/n): " confirmation
echo ""

if [[ $confirmation = [yY] ]]
then
	install_for_redhat
fi
;;

# this is debian
"debian")
echo "debian distro detected"

read -n 1 -p "install $whattoinstallname for debian? (y/n): " confirmation
echo ""

if [[ $confirmation = [yY] ]]
then
	install_for_debian
fi
;;

# this neither red hat nor debian
"unknown")
echo "this distro is neither red hat nor debian, installation is not possible."
;;

esac
}

# ===================

# get options
while getopts "abcd" opt
do
case $opt in
a) echo "found option $opt";;
b) echo "found option $opt";;
c) echo "found option $opt";;

d)
echo "debug mode on"
DEBUG=1
;;
esac
done

main_func
